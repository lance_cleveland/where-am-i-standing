/**
 * File: main.js
 *
 * The device interface handler.
 * 
 */
 
 
// Specific to my app
//
var myapp = new Object;
myapp.server = 'www.eatcharleston.com';    
  
// General settings
//
jQuery.support.cors = true;


// Things we do when the device is ready to process
//
function onDeviceReady() {
	navigator.geolocation.getCurrentPosition(whenCurrentPositionIsReturned, getCurrentPositionFailed);
	
	jQuery(document).delegate('.fullpage', 'pageshow', function () {
	    var the_height = (jQuery(window).height() - jQuery(this).find('[data-role="header"]').height() - jQuery(this).find('[data-role="footer"]').height());
	    jQuery(this).height(jQuery(window).height()).find('[data-role="content"]').height(the_height);
		}
	);
	
	jQuery.mobile.buttonMarkup.hoverDelay(100);	
	jQuery.mobile.defaultPageTransition('none');	
	jQuery.mobile.pushStateEnabled(false);	
}

// What we do when the current position is returned from the geolocator.
//
function whenCurrentPositionIsReturned(position) {
	jQuery('#pin').show();
	jQuery('#message').html(
				'You are standing on earth at<br/>'+
				position.coords.latitude + ' N/S (lat)<br/>' +
				position.coords.longitude + ' E/W (long)'
				);	
}

// What we do when getCurrentPosition returns an error.
function getCurrentPositionFailed(error) {
	jQuery('#message').html('Did you walk off the earth?<br/><em>'+error.code+'</em>');
}
